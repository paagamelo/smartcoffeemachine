package smart_cm_monitor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.TextArea;
import java.awt.TextField;
import javax.swing.*;

public class ViewImpl extends JFrame implements View {
	
	private static final long serialVersionUID = 1L;
	private final TextArea monitor;
	private final InputForm inputForm;
	
	public ViewImpl(ViewObserver observer) {
		monitor = new TextArea();
		inputForm = new InputForm(observer);
		monitor.setEditable(false);
		monitor.setText("");
		this.setLayout(new BorderLayout(5,5));
		this.add(monitor, BorderLayout.CENTER);
		this.add(inputForm, BorderLayout.PAGE_END);
		this.setMinimumSize(new Dimension(600, 480)); //don't try this at home
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Smart coffee machine monitor");
	}

	@Override
	public void showMessage(String msg) {
		monitor.setText(monitor.getText() + "\n" + msg);
	}
	
	class InputForm extends JComponent {
		
		private static final long serialVersionUID = 1L;
		private final TextField textArea = new TextField();
		private final JButton submit = new JButton("Submbit");
		
		InputForm(ViewObserver observer) {
			super();
			textArea.setPreferredSize(new Dimension(500, 24));
			submit.addActionListener(e -> observer.submitMessage(textArea.getText() + "\n"));
			this.setLayout(new FlowLayout());
			this.add(textArea);
			this.add(submit);
			this.setVisible(true);
		}
		
	}

}
