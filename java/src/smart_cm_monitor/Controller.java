package smart_cm_monitor;
import javax.swing.SwingUtilities;

public class Controller implements ViewObserver {
	
	private final View monitor;
	private SerialCommChannel channel;
	
	public Controller(String port, int baudRate) {
		monitor = new ViewImpl(this);
		try {
			channel = new SerialCommChannel(port, baudRate);
			new MachineOutputListenerAgent(monitor, channel).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void submitMessage(String msg) {
		channel.sendMsg(msg);
	}
	
	class MachineOutputListenerAgent extends Thread {
		
		private final View monitor;
		private SerialCommChannel channel;
		
		public MachineOutputListenerAgent(View monitor, SerialCommChannel channel) {
			this.monitor = monitor;
			this.channel = channel;
		}
		
		public void run() {
			while(true) {
				try {
					String msg = channel.receiveMsg();
					SwingUtilities.invokeLater(() ->  monitor.showMessage(msg));
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

}
