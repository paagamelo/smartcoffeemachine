package smart_cm_monitor;

public class SmartCoffeeMachineApp {
	
	public static void main(String...args) {
		if(args.length != 2) {
			new Controller("/dev/tty.usbmodem14501", 9600);
		} else {
			new Controller(args[0], Integer.parseInt(args[1]));
		}
	}

}
