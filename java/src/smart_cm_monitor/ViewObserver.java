package smart_cm_monitor;

public interface ViewObserver {
	
	void submitMessage(String msg);

}
