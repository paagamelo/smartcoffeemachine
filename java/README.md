Usage:
java -cp bin:lib/jSSC-2.8.0-Release/jssc.jar smart_cm_monitor.SmartCoffeeMachineApp portName baudRate

if portName and baudRate are not specified the program will assume "/dev/tty.usbmodem14501" as port
(first port of MacBook Pro 13' 2017) and 9600 as baud rate

to refill the machine you have to send a "refill" message 
