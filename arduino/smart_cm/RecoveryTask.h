#ifndef __RECOVERY_TASK__
#define __RECOVERY_TASK__

#include "Task.h"

class RecoveryTask: public Task {
	
protected:
	enum { IDLE, RECOVERY } state;

public:
	RecoveryTask();
	void init(int period);
	void tick();
	
};

#endif
