#ifndef __POTENTIOMETER__
#define __POTENTIOMETER__

#include "Selector.h"

class Potentiometer: public Selector {
	
protected:
	int pin;
	
public:
	Potentiometer(int pin);
	int getSelected();
	
};

#endif
