#include "DistanceMeasuringTask.h"
#include "Arduino.h"
#include "Config.h"

extern float measuredDistance;
extern machineState_t machine;

DistanceMeasuringTask::DistanceMeasuringTask(int trigPin, int echoPin) {
	this->sonar = new Sonar(trigPin, echoPin);
}

void DistanceMeasuringTask::init(int period) {
	Task::init(period);
  state = IDLE;
	count = ntimes;
	lastDistance = 0;
	measuredDistance = 5;
}

void DistanceMeasuringTask::tick() {
  switch(state) {
    case IDLE:
      if(shouldRun()) {
        state = ACTIVE;
      }
      break;
    case ACTIVE:
      if(!shouldRun()) {
        state = IDLE;
        /* 
         * this is to avoid a particular bug: 
         * if someone is very close while pressing the coffee button, this task will stop working 
         * leaving a measuredDistance < 0.10. Once the coffee is ready this task would wake up, but if the human 
         * got away meanwhile, the measuredDistance would stay < 0.10 for nTimes (to avoid false values), causing
         * the machine to consider the coffee as immediately taken.
         * This is the most simple solution found.
         */
        measuredDistance = 5;
      } else {
        float currentDistance = sonar->getDistance();
        if(currentDistance <= 0 || currentDistance > 4.50) {
          //just ignore that out of bounds value
        } else if(abs(currentDistance - lastDistance) > 0.5 && count < ntimes) {
          count++;
        } else {
          count = 0;
          lastDistance = currentDistance;
          measuredDistance = lastDistance;
        }
     } 
  }
	#ifdef __DEBUG__
  Serial.println();
	Serial.print("[D: " + String(measuredDistance) + "] ");
  #endif
}

bool DistanceMeasuringTask::shouldRun() {
  /*
   * normally we don't want this task to run if machine == STANDBY, but we need to do it
   * if we are using a MockPir (to fasten the tests)
   */
  return !(machine == MAKINGCOFFEE || machine == MAINTENANCE); //in every other state we want to measure the distance
}
	
