#ifndef __DISTANCE_MEASURING_TASK__
#define __DISTANCE_MEASURING_TASK__

#include "Task.h"
#include "Sonar.h"

class DistanceMeasuringTask: public Task {
	
protected:
  enum { IDLE, ACTIVE } state;
	DistanceDetector* sonar;
	float lastDistance;
	int count;
  /*
   * times before considering valid a deflected value, note that this number is to compare with
   * the task' frequency (a deflected value is valid after 2 times * 200 ms = 400 ms since its detecion)
   */
	const int ntimes = 2;
	
public:
	DistanceMeasuringTask(int trigPin, int echoPin);
	void init(int period);
	void tick();

private:
  bool shouldRun();

};

#endif
