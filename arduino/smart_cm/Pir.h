#ifndef __PIR__
#define __PIR__

#include "PresenceDetector.h"

class Pir: public PresenceDetector {

protected:
	int pin;	

public:
	Pir(int pin);
	bool isPresent();

};

#endif
