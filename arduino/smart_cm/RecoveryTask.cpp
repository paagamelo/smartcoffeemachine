#include "RecoveryTask.h"
#include "Arduino.h"
#include "Config.h"
#include "MsgService.h"

extern machineState_t machine;
extern int remaining;

RecoveryTask::RecoveryTask() {
}

void RecoveryTask::init(int period) {
	Task::init(period);
	state = IDLE;
  remaining = maxCoffee;
}

void RecoveryTask::tick() {
	switch(state) {
    case IDLE:
      if(machine == READY && remaining <= 0) {
        state = RECOVERY;
        machine = MAINTENANCE;
        MsgService.sendMsg("No more coffee. Waiting for recharge");
      }
      break;
		case RECOVERY:
			if(MsgService.isMsgAvailable()) {
        Msg* msg = MsgService.receiveMsg();    
        if(msg->getContent() == "refill") {
          state = IDLE;
          remaining = maxCoffee;
          MsgService.sendMsg("Coffee refilled: +" + String(remaining));
          machine = STANDBY; //reboot the machine
        }
      }
      break;
	}
  #ifdef __DEBUG__
  if(state == RECOVERY) {
    Serial.print("MAINTENANCE");
  }
  #endif
};
