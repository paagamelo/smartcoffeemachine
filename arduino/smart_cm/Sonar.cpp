#include "Sonar.h"
#include "Arduino.h"

Sonar::Sonar(int trigPin, int echoPin) {
	this->trigPin = trigPin;
	this->echoPin = echoPin;
	pinMode(trigPin, OUTPUT);
	pinMode(echoPin, INPUT);
}

float Sonar::getDistance() {
	digitalWrite(this->trigPin,LOW);
  delayMicroseconds(3);
  digitalWrite(this->trigPin,HIGH);
  delayMicroseconds(5);
  digitalWrite(this->trigPin,LOW);
    
  /* 
   * sonar may sometimes take phantom values (~20/30 m), in these occasion echo pin stays HIGH  
   * for a long time (up to 200 ms), causing overrun.
   * Adding a timeout of 30 ms should be sufficient to avoid the problem. With that time it is possible
   * to take a distance of 5 meters (way longer than the sonar capacity according to the datasheet).
   */
  float tUS = pulseIn(this->echoPin, HIGH, 30000); //30K us timeout
  float t = tUS / 1000.0 / 1000.0 / 2;
  float d = t*vs;
  return d;
};
