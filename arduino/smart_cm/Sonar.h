#ifndef __SONAR__
#define __SONAR__

#include "DistanceDetector.h"

class Sonar: public DistanceDetector {
	
protected:
	int trigPin;
	int echoPin;
	const float vs = 331.5 + 0.6*20;

public:
	Sonar(int trigPin, int echoPin);
	float getDistance();
	
};

#endif
