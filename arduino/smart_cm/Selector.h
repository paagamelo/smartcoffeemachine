#ifndef __SELECTOR__
#define __SELECTOR__

class Selector {
	
public:
	virtual int getSelected() = 0;
	
};

#endif
