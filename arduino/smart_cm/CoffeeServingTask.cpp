#include "CoffeeServingTask.h"
#include "Arduino.h"
#include "Config.h"
#include "MsgService.h"

extern float measuredDistance;
extern machineState_t machine;

CoffeeServingTask::CoffeeServingTask() {
}

void CoffeeServingTask::init(int period) {
	Task::init(period);
	count = 0;
	state = IDLE;
}

void CoffeeServingTask::tick() {
  switch(state) {
    case IDLE:
      if(machine == SERVINGCOFFEE) {
        state = MONITORING;
        count = 0;
      }
      break;
    case MONITORING:
      if(measuredDistance > coffeeTakingDistance && count < maxTimeToRemoveCoffee / myPeriod) {
        count++;
      } else if(measuredDistance <= coffeeTakingDistance || count >= maxTimeToRemoveCoffee / myPeriod) {
        state = IDLE;
        machine = READY;
        MsgService.sendMsg(measuredDistance < coffeeTakingDistance ? "[taken]" : "[trashed]");
      }
      break;
  }
  #ifdef __DEBUG__
  if(state == MONITORING) {
    Serial.print("SERVING COFFEE");
  }
  #endif
};
