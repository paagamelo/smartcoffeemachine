#include "Scheduler.h"
#include "Arduino.h"
#include <avr/sleep.h>
#include <avr/power.h>
#include "MsgService.h"

/* used just to compute wcet */
long wcet = 0;
int n = 0;

void Scheduler::init(int basePeriod){
  this->basePeriod = basePeriod;
  timer = new Timer();
  timer->setupPeriod(basePeriod);  
  nTasks = 0;
  MsgService.init();
}

bool Scheduler::addTask(Task* task){
  if (nTasks < MAX_TASKS-1){
    taskList[nTasks] = task;
    nTasks++;
    return true;
  } else {
    return false; 
  }
}

/*
 * Enter sleep mode, with Timer 1 active
 */
void Scheduler::sleep(){
  delay(50); /* fix needed to make it work */

  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();
  
  /* Disable all of the unused peripherals. This will reduce power
   * consumption further and, more importantly, some of these
   * peripherals may generate interrupts that will wake our Arduino from
   * sleep!
   */
  power_adc_disable();
  power_spi_disable();
  power_timer0_disable();
  power_timer2_disable();
  power_twi_disable();  

  /* Now enter sleep mode. */
  sleep_mode();  
  /* The program will continue from here after the timer timeout*/
  sleep_disable(); /* First thing to do is disable sleep. */
  /* Re-enable the peripherals. */
  power_all_enable();  
}

void Scheduler::run(){
  sleep();
  long ts = millis();
  /* waking up here */
  for (int i = 0; i < nTasks; i++){
    if (taskList[i]->updateAndCheckTime(basePeriod)){
      taskList[i]->tick();
    }   
  }
  #ifdef __WCET__
  long elapsed = millis() - ts;
  if(elapsed > wcet) {
    wcet = elapsed;
  }
  n++;
  if(n == 60 * 5) { //1 min
    Serial.println("WCET: " + String(wcet) + " + 50/70 ms for sleeping/waking up");
    while(1) {}; //blocks the program
  }
  #endif
}
