#include "PresenceDetector.h"
#include "Arduino.h"
#include "Sonar.h"

extern float measuredDistance;

class MockPir: public PresenceDetector {

public:
	MockPir() {
	}

  /*
   * note that the distance measuring task is normally not working when 
   * machine is in STANDBY, MockPir needs it to be active also in that state
   * in order to work correctly
   */
	bool isPresent() {
		return measuredDistance < 0.3;
	};
	
};
