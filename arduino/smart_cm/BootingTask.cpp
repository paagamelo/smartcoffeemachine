#include "BootingTask.h"
#include "Arduino.h"
#include "Config.h"
#include "MsgService.h"
#include <avr/power.h>

extern machineState_t machine;

BootingTask::BootingTask(int pirPin) {
	this->pir = new Pir(pirPin); //new MockPir(); <- use this to fasten tests
  MsgService.sendMsg("Calibrating..");
  delay(30000); //30 sec for pir calibration
}

void BootingTask::init(int period) {
	Task::init(period);
	countForEngagement = 0;
	countForPresence = 0;
  state = STANDBY_; //default starting state
  machine = STANDBY;
};

void BootingTask::tick() {
  switch(state) {
		case IDLE:
			if(machine == STANDBY) {
				state = STANDBY_;
			} else if(machine == READY) {
				state = READY_;
				countForEngagement = 0;
			}
      break;
		case STANDBY_:
			if(pir->isPresent()) {
				state = ON_;
        machine = ON;
				countForEngagement = 0;
				countForPresence = 0;
			}
      break;
		case ON_:
			if(pir->isPresent()) {
				countForPresence = 0;
			} else if(!pir->isPresent() && countForPresence < maxTimeWithNoPresence / myPeriod) {
				countForPresence++;
			} else if(!pir->isPresent() && countForPresence >= maxTimeWithNoPresence / myPeriod) {
				state = STANDBY_;
        machine = STANDBY;
			} 
			if(measuredDistance > engagementDistance) {
				countForEngagement = 0;
			} else if(measuredDistance <= engagementDistance && countForEngagement < minTimeForEngagement / myPeriod) {
				countForEngagement++;
			} else if(measuredDistance <= engagementDistance && countForEngagement >= minTimeForEngagement / myPeriod) {
				state = READY_;
        machine = READY;
				countForEngagement = 0;
        MsgService.sendMsg("Welcome!");
			}
      break;
		case READY_:
			if(machine != READY) { //machine is busy doing something
				state = IDLE;
			} else if(measuredDistance <= engagementDistance) {
				countForEngagement = 0;
			} else if(measuredDistance > engagementDistance && countForEngagement < maxTimeWithNoEngagement / myPeriod) {
				countForEngagement++;
			} else if(measuredDistance > engagementDistance && countForEngagement >= maxTimeWithNoEngagement / myPeriod) {
				state = ON_;
        machine = ON;
				countForPresence = 0;
				countForEngagement = 0;
			}
      break;
	}
  log();
};

/* just for debugging purpose */
void BootingTask::log() {
  #ifdef __DEBUG__
  String msg = "";
  String count = "";
  switch(state) {
    case STANDBY_: 
      msg = "STANDBY"; 
      break;
    case ON_: 
      msg = "ON";
      if(countForEngagement > 0) {
        count = " (countForEngagement: " + String(countForEngagement*myPeriod) + "/" + String(minTimeForEngagement) +")";
      } else if(countForPresence > 0) {
        count = " (countForNONPresence: " + String(countForPresence*myPeriod) + "/" + String(maxTimeWithNoPresence)+")";
      }
      break;
    case READY_:
      msg = "READY";
      if(countForEngagement > 0) {
        count = " (countForNONEngagement: " + String(countForEngagement*myPeriod) + "/" + String(maxTimeWithNoEngagement)+")";
      }
      break;
  }
  Serial.print(msg + count);
  #endif
}
		
			
	
