#include "Potentiometer.h"
#include "Arduino.h"

Potentiometer::Potentiometer(int pin) {
	this->pin = pin;
}

int Potentiometer::getSelected() {
	return analogRead(pin);
};
