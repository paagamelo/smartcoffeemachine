#ifndef __BOOTING_TASK__
#define __BOOTING_TASK__

#include "Task.h"
#include "MockPir.cpp"
#include "Pir.h"
#include "Sonar.h"

class BootingTask: public Task {
	
protected:
	PresenceDetector* pir;
	int countForPresence;
	int countForEngagement;
	enum { IDLE, STANDBY_, ON_, READY_ } state;
	
public:
	BootingTask(int pirPin);
	void init(int period);
	void tick();

private:
  void log();
	
};


#endif
