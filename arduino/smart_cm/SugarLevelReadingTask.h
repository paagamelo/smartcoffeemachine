#ifndef __SUGARLEVELREADING_TASK__
#define __SUGARLEVELREADING_TASK__

#include "Task.h"
#include "Potentiometer.h"

class SugarLevelReadingTask: public Task {
	
protected:
	Selector* potentiometer;
	int level;
	enum { IDLE, READING } state;
	
public:
	SugarLevelReadingTask(int pin);
	void init(int period);
	void tick();
	
private:
	void printLevel();

};

#endif
