#ifndef __CONFIG__
#define __CONFIG__

//#define __DEBUG__ /* enable to see every task state at every clock */
//#define __WCET__ /* enable to compute the wcet in 1 min of execution */

#define PIRPIN 3
#define TRIGPIN 7
#define ECHOPIN 13
#define LED1PIN 8
#define LED2PIN 9
#define LED3PIN 10
#define T1PIN 2
#define POTPIN A5

typedef enum { STANDBY, ON, READY, MAKINGCOFFEE, SERVINGCOFFEE, MAINTENANCE } machineState_t;

const int maxCoffee = 2;

const float engagementDistance = 0.4; // DIST1 in meters

const float coffeeTakingDistance = 0.1; //DIST2 in meters

const int minTimeForEngagement = 1000; //DT1 in milliseconds

const int maxTimeWithNoEngagement = 5000; //DT2a in millisecons

const int maxTimeWithNoPresence = 5000; //DT2b in milliseconds

const int coffeeMakingDuration = 3000; //DT3 in milliseconds

const int maxTimeToRemoveCoffee = 5000; //DT4 in milliseconds

#endif
