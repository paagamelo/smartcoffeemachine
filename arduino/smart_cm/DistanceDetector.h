#ifndef __DISTANCE_DETECTOR__
#define __DISTANCE_DETECTOR__

class DistanceDetector {
	
public:
	virtual float getDistance() = 0;

};

#endif
