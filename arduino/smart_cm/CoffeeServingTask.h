#ifndef __COFFEE_SERVING_TASK__
#define __COFFEE_SERVING_TASK__

#include "Task.h"

class CoffeeServingTask: public Task {

protected:
	enum { IDLE, MONITORING } state;
	int count;

public:
	CoffeeServingTask();
	void init(int period);
	void tick();
	
};

#endif
