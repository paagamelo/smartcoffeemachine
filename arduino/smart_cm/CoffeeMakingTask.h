#ifndef __COFFEE_MAKING_TASK__
#define __COFFEE_MAKING_TASK__

#include "Task.h"
#include "Led.h"
#include "ButtonImpl.h"

class CoffeeMakingTask: public Task {
	
protected:
  Button* button;
	Light* led[3];
	enum { IDLE, MONITORING, MAKINGCOFFEE_ } state;
	int count;
  const int nLeds = 3;

public:
	CoffeeMakingTask(int led1, int led2, int led3, int buttonpin);
	void init(int period);
	void tick();
	
private:
  int ledsOn;
	void switchAllOf();
  bool buttonIsPressed();
	
};

#endif
