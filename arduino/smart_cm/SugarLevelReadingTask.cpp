#include "SugarLevelReadingTask.h"
#include "Arduino.h"
#include "Config.h"

#define MIN 1
#define MAX 5

extern machineState_t machine;

SugarLevelReadingTask::SugarLevelReadingTask(int pin) {
	this->potentiometer = new Potentiometer(pin);
}

void SugarLevelReadingTask::init(int period) {
	Task::init(period);
	state = IDLE;
	level = MIN;
}

void SugarLevelReadingTask::tick() {
	switch(state) {
		case IDLE:
			if(machine == READY) {
				state = READING;
			}
			break;
		case READING:
			if(machine != READY) {
				state = IDLE;
			} else {
				int value = map(potentiometer->getSelected(), 1, 1019, MIN, MAX);
				if(level != value) {
					level = value;
					printLevel();
				}
			}
			break;
	}
};

/* just printing */
void SugarLevelReadingTask::printLevel() {
  Serial.print("Sugar: ");
  int i;
	for(i=MIN; i<=level; i++) {
		Serial.print("* ");
	}
	for(; i <= MAX; i++) {
		Serial.print("- ");
	}
  Serial.println();
};
