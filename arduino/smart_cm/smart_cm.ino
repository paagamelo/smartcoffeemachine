#include "Config.h"
#include "Scheduler.h"
#include "DistanceMeasuringTask.h"
#include "BootingTask.h"
#include "RecoveryTask.h"
#include "SugarLevelReadingtask.h"
#include "CoffeeMakingTask.h"
#include "CoffeeServingTask.h"

float measuredDistance; //current human distance
int remaining; //number of remaining coffees
machineState_t machine;

Scheduler sched;

void setup() {
  sched.init(200);
  
  Task* distanceMeasuring = new DistanceMeasuringTask(TRIGPIN, ECHOPIN);
  distanceMeasuring->init(200);
  Task* booting = new BootingTask(PIRPIN);
  booting->init(200);
  Task* recovery = new RecoveryTask();
  recovery->init(200);
  Task* sugarLevelReading = new SugarLevelReadingTask(POTPIN);
  sugarLevelReading->init(400);
  Task* coffeeMaking = new CoffeeMakingTask(LED1PIN, LED2PIN, LED3PIN, T1PIN);
  coffeeMaking->init(200);
  Task* coffeeServing = new CoffeeServingTask();
  coffeeServing->init(200);
 
  sched.addTask(distanceMeasuring);
  sched.addTask(booting);
  sched.addTask(sugarLevelReading);
  sched.addTask(coffeeMaking);
  sched.addTask(coffeeServing);
  sched.addTask(recovery);
}

void loop() {
  sched.run();
}
