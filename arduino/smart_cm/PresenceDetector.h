#ifndef __PRESENCE_DETECTOR__
#define __PRESENCE_DETECTOR__

class PresenceDetector {

public:
	virtual bool isPresent() = 0;

};

#endif
