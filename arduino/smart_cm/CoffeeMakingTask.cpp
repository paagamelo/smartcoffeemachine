#include "CoffeeMakingTask.h"
#include "Arduino.h"
#include "Config.h"
#include "MsgService.h"

extern machineState_t machine;
extern int remaining;

CoffeeMakingTask::CoffeeMakingTask(int led1Pin, int led2Pin, int led3Pin, int buttonpin) {
	led[0] = new Led(led1Pin);
  led[1] = new Led(led2Pin);
  led[2] = new Led(led3Pin);
  button = new ButtonImpl(buttonpin);
}

void CoffeeMakingTask::init(int period) {
	Task::init(period);
	state = IDLE;
	count = 0;
  ledsOn = 0;
	switchAllOf();
}

void CoffeeMakingTask::tick() {
  switch(state) {
    case IDLE:
      if(machine == READY) {
        state = MONITORING;
      }
		case MONITORING:
      if(machine != READY) {
        state = IDLE;
      } else if(remaining > 0 && buttonIsPressed()) {
        state = MAKINGCOFFEE_;
        machine = MAKINGCOFFEE;
        count = coffeeMakingDuration / nLeds / myPeriod;
        MsgService.sendMsg("Making a coffee");
      }
			break;
		case MAKINGCOFFEE_:
      if(count < coffeeMakingDuration / nLeds / myPeriod) {
				count++;
			} else if(ledsOn < nLeds && count >= coffeeMakingDuration / nLeds / myPeriod) {
        led[ledsOn]->switchOn();
        ledsOn++;
				count = 0;
			} else if(ledsOn == nLeds && count >= coffeeMakingDuration / nLeds / myPeriod) {
        count = 0;
        switchAllOf();
        ledsOn = 0;
        state = IDLE;
        machine = SERVINGCOFFEE;
        remaining--;
        MsgService.sendMsg("The coffee is ready");
      }
			break;
	}
  #ifdef __DEBUG__
  if(state == MAKINGCOFFEE_) {
    Serial.print("MAKING COFFEE");
  }
  #endif
}

/*
 * This task has a frequency of 200 ms in order to achieve a *real* power saving
 * (keeping waking up the cpu at a frequency < 200 just consume power).
 * However checking for the pression of the button 5 times in a second can be dangerous: we may lost
 * some input. The solution is a little tricky: we keep checking for user input for 100 ms every 200 ms,
 * note that during this operation timer is counting, so we are just checking for input every ~100 ms.
 */
bool CoffeeMakingTask::buttonIsPressed() {
  long ts = millis();
  while(millis() - ts < 100) {
    if(button->isPressed()) {
      return true;
    }
  }
  return false;
}

void CoffeeMakingTask::switchAllOf() {
	for(int i=0; i<3; i++) {
		led[i]->switchOff();
	}
};
